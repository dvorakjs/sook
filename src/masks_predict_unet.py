###################################################
#
#   Script to
#   - Calculate prediction of the test dataset
#   - Calculate the parameters to evaluate the prediction
#
##################################################

#Python
import numpy as np
import ConfigParser
from matplotlib import pyplot as plt
import glob
import os
import ntpath

#Keras
from keras.models import model_from_json
from keras.models import Model
#scikit learn
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import jaccard_similarity_score
from sklearn.metrics import f1_score
import sys
sys.path.insert(0, './lib/')
# help_functions.py
from help_functions import *
# extract_patches.py
from extract_patches_rev import recompone
from extract_patches_rev import recompone_overlap
from extract_patches_rev import paint_border
from extract_patches_rev import kill_border
from extract_patches_rev import pred_whole_img
# pre_processing.py
from pre_processing_v2 import my_PreProc


#==========================================



patch_height =128
patch_width =128

average_mode = True
stride_height = 4 
stride_width = 4
assert (stride_height < patch_height and stride_width < patch_width)

N_visual = 1
# batch_size = 12

name_experiment = 'test_derma'
path_experiment = './' + name_experiment + '/'

#===========================================
dataset_path = './input/'

IMG_FOLDER = './test/256/images3456/'
DEBUG_FOLDER = './debug/'


# =========== Load the saved model
best_last = 'best'
#best_last = 'last'
model = model_from_json(open(path_experiment+name_experiment + '_' + str(patch_height) + '_' + str(patch_width) + '_architecture.json').read())
a = path_experiment+name_experiment + '_' + str(patch_height) + '_' + str(patch_width) + '_'+best_last+'_weights.h5'
print a
model.load_weights(path_experiment+name_experiment + '_' + str(patch_height) + '_' + str(patch_width) + '_'+best_last+'_weights.h5')


#--------------- run ------------------------------------------------------------------------- 
def paint_border_overlap(full_img, patch_h, patch_w, stride_h, stride_w):

    img_h = full_img.shape[2]  
    img_w = full_img.shape[3] 
    leftover_h = (img_h-patch_h)%stride_h  #leftover on the h dim
    leftover_w = (img_w-patch_w)%stride_w  #leftover on the w dim

    if (leftover_h != 0):  #change dimension of img_h
        print "\nthe side H is not compatible with the selected stride of " +str(stride_h)
        print "img_h " +str(img_h) + ", patch_h " +str(patch_h) + ", stride_h " +str(stride_h)
        print "(img_h - patch_h) MOD stride_h: " +str(leftover_h)
        print "So the H dim will be padded with additional " +str(stride_h - leftover_h) + " pixels"
        
    tmp_full_img = np.zeros((full_img.shape[0], full_img.shape[1], img_h+(stride_h-leftover_h), img_w))
    tmp_full_img[0:full_img.shape[0], 0:full_img.shape[1], 0:img_h, 0:img_w] = full_img
    full_img = tmp_full_img

    if (leftover_w != 0):   #change dimension of img_w
        print "the side W is not compatible with the selected stride of " +str(stride_w)
        print "img_w " +str(img_w) + ", patch_w " +str(patch_w) + ", stride_w " +str(stride_w)
        print "(img_w - patch_w) MOD stride_w: " +str(leftover_w)
        print "So the W dim will be padded with additional " +str(stride_w - leftover_w) + " pixels"
        
    tmp_full_img = np.zeros((full_img.shape[0], full_img.shape[1], full_img.shape[2], img_w+(stride_w - leftover_w)))
    tmp_full_img[0:full_img.shape[0], 0:full_img.shape[1], 0:full_img.shape[2], 0:img_w] = full_img
    full_img = tmp_full_img
   
    print 'exteneded image shape: ', str(full_img.shape)

    return full_img


def extract_ordered_overlap(full_img, patch_h, patch_w, stride_h, stride_w):

    img_h = full_img.shape[2]  
    img_w = full_img.shape[3] 
    assert ((img_h-patch_h)%stride_h==0 and (img_w-patch_w)%stride_w==0)

    num_patches_h = ((img_h-patch_h)//stride_h+1) #// --> division between integers
    num_patches_w = ((img_w-patch_w)//stride_w+1)
    N_patches_img = num_patches_h * num_patches_w  
    print "Number of patches on h : " +str(num_patches_h)
    print "Number of patches on w : " +str(num_patches_w)
    print "number of patches per image: " +str(N_patches_img) 

    patches = np.empty((N_patches_img, full_img.shape[1], patch_h, patch_w))
    iter_tot = 0   #iter over the total number of patches (N_patches)

    for i in range(full_img.shape[0]):  #loop over the full images
        for h in range((img_h-patch_h)//stride_h+1):
            for w in range((img_w-patch_w)//stride_w+1):
                patch = full_img[i,:,h*stride_h:(h*stride_h)+patch_h,w*stride_w:(w*stride_w)+patch_w]
                patches[iter_tot]=patch
                iter_tot +=1   #total

    assert (iter_tot==N_patches_img)

    return patches, num_patches_h, num_patches_w  #array with all the full_imgs divided in patches



def get_patches_from_image(img, patch_height, patch_width, stride_height=1, stride_width=1):
    
    # extend an original image into a new image whose size get larger if necessary due to the stride of the sliding patch
    img_padded = paint_border_overlap(img, patch_height, patch_width, stride_height, stride_width)
    #img_padded = img

    patches, num_patches_h, num_patches_w = extract_ordered_overlap(img_padded, patch_height, patch_width, stride_height, stride_width)
    #print 'patch shapes: ', str(patches.shape)

    return patches, num_patches_h, num_patches_w, img_padded.shape[2], img_padded.shape[3]



#--------------- run ------------------------------------------------------------------------- 
for path, subdirs, files in os.walk(IMG_FOLDER):

    print '\nFolder: ', path

    if files: 
        for i in range(len(files)):

            print files[i], '\n'
            full_filenm = os.path.join(path, files[i])
            img = Image.open(full_filenm)

            # enlarge the imge
            #ratio = 1.1
            #img = img.resize((625, 625), Image.ANTIALIAS)

            # check if reading an image is successful
            #img.show()

            img = np.asarray(img)
            if img.ndim != 3:
                print 'RGB color image only'
                exit()

            img = np.transpose(img, (2,0,1))    # make channel comes first
            height = img.shape[1]
            width = img.shape[2]
            print 'image shape: ', str(img.shape)

            # convert 3D image format into 4D format to resuse library functions that accept 4D array only
            img = np.reshape(img, (1, img.shape[0], img.shape[1], img.shape[2]))
            # preprocessing
            img = my_PreProc(img)

            # make a set of patches on the image in sliding window fansion 
            patches, num_patch_h, num_patch_w, new_height, new_width = get_patches_from_image(img, patch_height, patch_width, stride_height, stride_width)
            print 'patchs shape: ', str(patches.shape)

            # patch-by-patch pre-processing: 
            # patches = my_PreProc(patches)


            # debug: check the patches
            '''
            for i in range(patches.shape[0]):
                array = 255 * patches[i,0]
                thumbnail = Image.fromarray(array.astype(np.uint8))
                thumbnail.save( DEBUG_FOLDER + 'patches/' + str(i) + '_patch.gif')
            '''
            
            # classifications on each patch
            predictions = model.predict(patches, batch_size=1, verbose=2)
            print 'the size of the output of CNN: ', str(predictions.shape)

            # Convert the prediction arrays to the figure ground in corresponding images 
            pred_patches = pred_to_imgs(predictions, patch_height, patch_width, "original")
            print 'shape of the converted patch predictions: ', str(pred_patches.shape)

            # generate a new image based on the prediction results
            if average_mode == True:   # combine all of each prediction results into a single image by averaging them
                pred_each_img = recompone_overlap(i, pred_patches, new_height, new_width, stride_height, stride_width)# predictions
                pred_each_img = pred_each_img[:,:,0:height, 0:width]
    
            else:
                print "no other mode than average mode is supported"
            filenm = os.path.splitext(files[i])[0]
            visualize(group_images(pred_each_img, 1), path_experiment + filenm + "_pred")#.show()

