﻿###################################################
#
#   Script to:
#   - Load the images and extract the patches
#   - Define the neural network
#   - define the training
#
##################################################


import numpy as np
import ConfigParser

from keras.models import Model
from keras.layers import Input, merge, concatenate, Conv2D, MaxPooling2D, UpSampling2D, AveragePooling2D, Reshape, core, Dropout, Dense, Activation, Flatten
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint, LearningRateScheduler

import theano

from keras import backend as K

#from keras.utils.visualize_util import plot
from keras.optimizers import SGD

import sys
sys.path.insert(0, './lib/')
from help_functions import *

#function to obtain data for training/testing (validation)
from extract_patches import get_data_training
#import os    
#os.environ['THEANO_FLAGS'] = "device=cuda"  

#import theano
#theano.config.device = 'gpu'
#theano.config.floatX = 'float32'
#========= Load settings from Config file

patch_height = 256 
patch_width = 256

N_subimg = 	277
# total # of patches. It should be a multiple of training images, 1000, 200 patches per image
N_epochs = 10000

batch_size = 35

path_data = './input/'
name_experiment = 'test_derma'
path_experiment = './'+name_experiment + '/'

best_last = 'best'
#best_last = 'last'


train_imgs_original = 'angio_dataset_imgs_derma_train.hdf5'
train_groundTruth = 'angio_dataset_groundTruth_derma_train.hdf5'

DRIVE_train_imgs_original = path_data + train_imgs_original
DRIVE_train_groudTruth = path_data + train_groundTruth

inside_FOV = False

smooth = 1.

def dice_coef(y_true, y_pred):
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    return (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)

def dice_coef_loss(y_true, y_pred):
    return -dice_coef(y_true, y_pred)


# ========= Define the neural network
def get_unet(n_ch,patch_height,patch_width):
    print K.image_dim_ordering() 
    if K.image_dim_ordering() == 'th':
       channel_axis = 1
       inputs = Input((n_ch, patch_height, patch_width))
    else:
       channel_axis = 3
       inputs = Input((patch_height, patch_width, n_ch))

    print('K.image_dim_ordering = {} Channel axis = {}'.format(K.image_dim_ordering(), channel_axis))

    conv1 = Conv2D(32, (3,3), activation='relu', border_mode='same')(inputs)
    conv1 = Dropout(0.3)(conv1)
    conv1 = Conv2D(32, (3,3), activation='relu', border_mode='same')(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

    #
    conv2 = Conv2D(64, (3,3), activation='relu', border_mode='same')(pool1)
    conv2 = Dropout(0.3)(conv2)
    conv2 = Conv2D(64, (3,3), activation='relu', border_mode='same')(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

    #
    conv3 = Conv2D(128, (3,3), activation='relu', border_mode='same')(pool2)
    conv3 = Dropout(0.3)(conv3)
    conv3 = Conv2D(128, (3,3), activation='relu', border_mode='same')(conv3)

    #up1 = merge([UpSampling2D(size=(2, 2))(conv3), conv2], mode='concat', concat_axis=1)
    #up1 = concatenate([UpSampling2D(size=(2, 2))(conv3), conv2], axis=channel_axis)

    conv4 = Conv2D(64, (3,3), activation='relu', border_mode='same')(conv3)
    conv4 = Dropout(0.3)(conv4)
    conv4 = Conv2D(64, (3,3), activation='relu', border_mode='same')(conv4)

    #up2 = merge([UpSampling2D(size=(2, 2))(conv4), conv1], mode='concat', concat_axis=1)
    #up2 = concatenate([UpSampling2D(size=(2, 2))(conv4), conv1], axis=channel_axis)

    conv5 = Conv2D(32, (3,3), activation='relu', border_mode='same')(conv4)
    conv5 = Dropout(0.3)(conv5)
    conv5 = Conv2D(32, (3,3), activation='relu', border_mode='same')(conv5)

    #

    conv6 = Conv2D(2, (1,1), activation='relu',border_mode='same')(conv5)
    conv6 = core.Reshape((2,patch_height*patch_width))(conv6)
    conv6 = core.Permute((2,1))(conv6)

    conv7 = Flatten()(conv6)
    conv8 = Dense(256, activation = 'relu')(conv7)
    conv9 = Dense(128, activiation = 'relu')(conv8)
    conv10 = Dense(4, activation = 'softmax')(conv9)

    model = Model(input = inputs, output = conv10)
    model.compile(optimizer = 'sgd', loss = 'categorical_crossentropy', metrics = ['accuracy'])
    
    return model




#============ Load the data and divided in patches
# no preprocessing, raw RGB color 
patches_imgs_train, patches_classes = get_data_training(DRIVE_train_imgs_original, DRIVE_train_groudTruth)

#print 'patches_imgs_train====', patches_imgs_train.shape 
#========= Save a sample of what you're feeding to the neural network ==========
#N_sample = min(patches_imgs_train.shape[0],40)
#visualize(group_images(patches_imgs_train[0:N_sample,:,:,:],5),'./'+name_experiment+'/'+"sample_input_imgs")#.show()
#visualize(group_images(patches_masks_train[0:N_sample,:,:,:],5),'./'+name_experiment+'/'+"sample_input_masks")#.show()


#=========== Construct and save the model arcitecture =====
n_ch = patches_imgs_train.shape[1]
patch_height = patches_imgs_train.shape[2]
patch_width = patches_imgs_train.shape[3]

print n_ch

model = get_unet(n_ch, patch_height, patch_width)  #the U-net model
print "Check: final output of the network:"
print model.output_shape

#plot(model, to_file='./'+name_experiment+'/'+name_experiment + '_model.png')   #check how the model looks like
json_string = model.to_json()
open('./'+name_experiment+'/'+name_experiment + '_' + str(patch_height) + '_' + str(patch_width) + '_architecture_classifier.json', 'w').write(json_string)


#============  Training ==================================
checkpointer = ModelCheckpoint(filepath='./'+name_experiment+'/'+name_experiment + '_' + str(patch_height) + '_' + str(patch_width) + '_best_weights_classifier.h5', verbose=1, monitor='val_loss', mode='auto', save_best_only=True) #save at each epoch if the validation decreased

#patches_masks_train = masks_Unet(patches_masks_train)  # make the target values from the mask info.
model.summary()

model.fit(patches_imgs_train, patches_classes, nb_epoch=N_epochs, batch_size = batch_size, verbose=2, shuffle=True, validation_split=0.2, callbacks=[checkpointer])



#========== Save and test the last model ===================
model.save_weights('./'+name_experiment+'/'+name_experiment + '_' + str(patch_height) + ' ' + str(patch_width) + '_last_weights_no_upsampling_classifier.h5', overwrite=True)





