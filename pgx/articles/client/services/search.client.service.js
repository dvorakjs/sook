(function () {
  'use strict';

  angular
    .module('articles.services')
    .factory('SearchService', SearchService);

  SearchService.$inject = ['$resource', '$log'];

  function SearchService($resource, $log) {
    var Article = $resource('/api/find/:keyword', {keyword:'@keyword'}, {find:{method:'GET'}});

    angular.extend(Article.prototype, {
      search: function () {
        var article = this;
        return search(article);
      }
    });

    return Article;

    function search(article) {

      if (article.keyword) {
        return article.$find(onSuccess, onError);
      }

      // Handle successful response
      function onSuccess(article) {
        // Any required internal processing from inside the service, goes here.
      }

      // Handle error response
      function onError(errorResponse) {
        var error = errorResponse.data;
        // Handle error internally
        handleError(error);
      }
    };

     

    function handleError(error) {
      // Log error
      $log.error(error);
    }
  }
}());
