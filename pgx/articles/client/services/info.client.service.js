(function () {
  'use strict';

  angular
    .module('articles.services')
    .factory('InfoService', InfoService);

  InfoService.$inject = ['$resource', '$log'];

  function InfoService($resource, $log) {
    var Article = $resource('/api/info/', {gene:'@gene'}, {genotype:'@genotype'}, {getInfo:{method:'GET'}});

    angular.extend(Article.prototype, {
      getDataInfo: function () {
        var article = this;
        return getDataInfo(article);
      }
    });

    return Article;

    function getDataInfo(article) {

      return article.$getInfo(onSuccess, onError);
      
      // Handle successful response
      function onSuccess(article) {
        // Any required internal processing from inside the service, goes here.
      }

      // Handle error response
      function onError(errorResponse) {
        var error = errorResponse.data;
        // Handle error internally
        handleError(error);
      }
    };

    function handleError(error) {
      // Log error
      $log.error(error);
    }
  }
}());
