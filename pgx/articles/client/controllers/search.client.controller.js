(function () {
  'use strict';

  angular
    .module('articles', ['angular.filter'])
    .controller('SearchController', SearchController);

  SearchController.$inject = ['$scope', '$state', 'searchResolve', 'Notification', '$uibModal'];

  function SearchController($scope, $state, article, Notification, $uibModal) {
    	var vm = this;
    
    	vm.article = article;
    	$scope.searchData = false;
        
        var pops = [];
        var mData = []; 
    	
	$scope.keywordSearch = function(){
  		var word = angular.copy($scope.kw);
		if(word){
  			$scope.noKeyword = "";
                	vm.article.keyword = word;
			vm.article.search()
				.then(successCallback)
				.catch(errorCallback);

 			function successCallback(res){
				$state.go('find');
				var rows = res.Rows;
             			if(rows.length == 0){
			 		$scope.searchData = false;
					Notification.success({message: 'No data'});

				}else{
					$scope.searchData = true;
                                        classify(rows);
					$scope.mData = mData;
					$scope.pops = pops; 

				}
			}
			
			function errorCallback(res){
				$state.go('find');
				Notification.error({message: res.data.message, title:'Error'});
			}

		}else{
			$scope.searchData = false;
	 		$scope.noKeyword = "Keyword required";
        	}
	}

	$scope.modalOpen = function(param){
		$uibModal.open({templateUrl:'/modules/articles/client/views/info.client.view.html',
				size: 'lg',
				controller: ["$scope", "$uibModalInstance", function ($scope, $uibModalInstance){
                                $scope.pop = pops[param.index];  
                  		$scope.ok =function() {
                       		$uibModalInstance.close();
                  		};}]
		});     		

	}
       function classify(rows){
        var rData = copy(rows[0], 0);
        
        var mIndex = 0;
        mData[mIndex] = rData;
        var fun = getFunctions(rData.GENOTYP);

        var pData = [];
        var pIndex = 0;
	var popIndex = 0;

	for(var i = 1; i < rows.length; i++){
		if(compare(rData, rows[i])){
		   if(fun.indexOf(rows[i].FUNC, 0) >= 0){
		   	var pd = pCopy(rows[i]);
			pData[pIndex] = pd;
			pIndex++;
			

                   }	
				                                    					
		}else{ 
                        mIndex = mIndex+1;
			rData = copy(rows[i], mIndex);
			mData[mIndex] = rData;
			fun = [];	
			fun = getFunctions(mData[mIndex].GENOTYP);
			pops[popIndex] = pData;
			popIndex++;
			
			pData = [];
			pIndex = 0;
			
			
                }
         }
	 pops[popIndex] = pData;
       }

      
      


     function compare(rData, nData){
         if(rData.GENE_NM == nData.GENE_NM && rData.PHENOTYP == nData.PHENOTYP && rData.ACTIVITY_SCORE == nData.ACTIVITY_SCORE && rData.GENOTYP == nData.GENOTYP && rData.EX_DIPLOTYP == nData.EX_DIPLOTYP && rData.IMPLICATION == nData.IMPLICATION && rData.THERAPY_RECOM == nData.THERAPY_RECOM && rData.RECOM_CLASS == nData.RECOM_CLASS && rData.OTHER_RECOM_CLASS == nData.OTHER_RECOM_CLASS && rData.OTHER_RECOM_TYP == nData.OTHER_RECOM_TYP && rData.DRUG_GROUP == nData.DRUG_GROUP) return true; else return false; 
	
      }

    function copy(nData, index){
         var rData = {};

         rData.GENE_NM = nData.GENE_NM;
         rData.PHENOTYP = nData.PHENOTYP;
         rData.ACTIVITY_SCORE = nData.ACTIVITY_SCORE;
         rData.GENOTYP = nData.GENOTYP;
         rData.EX_DIPLOTYP = nData.EX_DIPLOTYP;
         rData.IMPLICATION = nData.IMPLICATION;
         rData.THERAPY_RECOM = nData.THERAPY_RECOM;
         rData.RECOM_CLASS = nData.RECOM_CLASS;
         rData.OTHER_RECOM_CLASS = nData.OTHER_RECOM_CLASS;
         rData.OTHER_RECOM_TYP = nData.OTHER_RECOM_TYP;
         rData.DRUG_GROUP = nData.DRUG_GROUP;
	 rData.INDEX = index;
	 return rData; 
    }

   function getFunctions(genotype){
      var fun = [];
      var fIndex = 0;


      if(genotype.includes('decreased-function') || genotype.includes('decreased function')){
      	fun[fIndex] = "decreased function";
	fIndex++;}
     if(genotype.includes('increased-function') || genotype.includes('increased function')){
        fun[fIndex] = "increased function";
	fIndex++;}
     if(genotype.includes('no-function') || genotype.includes('no function')){
        fun[fIndex] = "no function";
	fIndex++;}
     if(genotype.includes('normal-function') || genotype.includes('normal function')){
	fun[fIndex] = "normal function"; 
	fIndex++;}
     if(genotype.includes('reduced-function') || genotype.includes('reduced function')){
	fun[fIndex] = "reduced function"; 
	fIndex++;}
     if(genotype.includes('loss-of-function'))
	fun[fIndex] = "loss-of-function";

	
     return fun;
  }



    function pCopy(rData){
         var pData = {};

         pData.GENE_NM = rData.GENE_NM;
         pData.REP_ALLEL_NM = rData.REP_ALLEL_NM;
         pData.ALLEL_DTL_NM = rData.ALLEL_DTL_NM;
	 pData.FUNC = rData.FUNC;
         pData.REF_PAPER = rData.REF_PAPER;
         pData.SNP_NO = rData.SNP_NO;
         return pData;
    }







  }
}());
