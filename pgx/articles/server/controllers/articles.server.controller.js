'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Article = mongoose.model('Article'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  mysql = require('mysql');
/**
 * Create an article
 */
var  mysqlHost = '13.209.76.41',
     mysqlUser = 'annot',
     mysqlPassword = '?4321?',
     mysqlDatabase = 'PGX';

exports.create = function (req, res) {
  var article = new Article(req.body);
  article.user = req.user;

  article.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(article);
    }
  });
};

/**
 * Show the current article
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var article = req.article ? req.article.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  article.isCurrentUserOwner = !!(req.user && article.user && article.user._id.toString() === req.user._id.toString());

  res.json(article);
};

/**
 * Update an article
 */
exports.update = function (req, res) {
  var article = req.article;

  article.title = req.body.title;
  article.content = req.body.content;

  article.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(article);
    }
  });
};

/**
 * Delete an article
 */
exports.delete = function (req, res) {
  var article = req.article;

  article.remove(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(article);
    }
  });
};

/**
 * List of Articles
 */
exports.list = function (req, res) {
  Article.find().sort('-created').populate('user', 'displayName').exec(function (err, articles) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(articles);
    }
  });
};



/**
 * List of Articles
 */
exports.find = function (req, res) {

        var Keyword = req.params.keyword;
      
	if(String(Keyword) == 'undefined'){
		return res.send({no_data:0});
	}


	var connection = mysql.createConnection({
		host: mysqlHost,
		user: mysqlUser,
		password: mysqlPassword,
                database: mysqlDatabase
	});

        
	connection.connect(function(err){
		if(err){console.log("Error: not connected", err);
			return res.status(422).send({message: errorHandler.getErrorMessage(err)});
	
		}
		else{
			console.log("DB connected");
			
			var queryString = "SELECT REL.DRUG_GROUP, GENE.GENE_NM "+
						",REL_DTL.PHENOTYP, REL_DTL.ACTIVITY_SCORE, REL_DTL.GENOTYP "+
						",REL_DTL.EX_DIPLOTYP, REL_DTL.IMPLICATION, REL_DTL.THERAPY_RECOM, REL_DTL.RECOM_CLASS, REL_DTL.OTHER_RECOM_CLASS"+
						",REL_DTL.OTHER_RECOM_TYP,  STAR.REP_ALLEL_NM, DTL.ALLEL_DTL_NM, DTL.FUNC"+
						",DTL.REF_PAPER"+	 
						" FROM GENE_DRUG_REL REL "+
						" LEFT JOIN GENE_DRUG_REL_DTL REL_DTL ON (REL.GENE_DRUG_REL_ID = REL_DTL.GENE_DRUG_REL_ID) "+
						" LEFT JOIN GENE GENE ON (REL.GENE_ID = GENE.GENE_ID) " +
						" LEFT JOIN STAR_ALLEL STAR ON (REL.GENE_ID = STAR.GENE_ID) " +
						" LEFT JOIN STAR_ALLEL_DTL DTL ON (STAR.ALLEL_NO = DTL.ALLEL_NO) " +
						" LEFT JOIN PGKB_DRUG PD ON (REL.PGKB_DRUG_ID = PD.PGKB_DRUG_ID) "+
						" WHERE INSTR(PD.TRADE_NM, ?) OR INSTR(REL.DRUG_GROUP, ?)" +
						" ORDER BY REL.DRUG_GROUP, GENE.GENE_NM, REL_DTL.PHENOTYP"; 




		        connection.query(queryString,[Keyword, Keyword],  function(err, rows, fields){
	        	       	if(err){
					console.log('Error!', err);
				}else{
					connection.end();
					console.log(rows);
					return res.send({Rows:rows});
				}
			});

		}

	});

};


exports.getInfo = function(req, res){
        return res.send({FUNC:'AA'});

};
/**
 * Article middleware
 */
exports.articleByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Article is invalid'
    });
  }

  Article.findById(id).populate('user', 'displayName').exec(function (err, article) {
    if (err) {
      return next(err);
    } else if (!article) {
      return res.status(404).send({
        message: 'No article with that identifier has been found'
      });
    }
    req.article = article;
    next();
  });
};
