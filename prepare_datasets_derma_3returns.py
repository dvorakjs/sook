#==========================================================
#
#  This prepare the hdf5 datasets of the database
#
#============================================================

import os
import h5py
import numpy as np
from PIL import Image



def write_hdf5(arr,outfile):
  with h5py.File(outfile,"w") as f:
    f.create_dataset("image", data=arr, dtype=arr.dtype)

channels = 3
height = 256		# 512x512 is the size of original images
width = 256
imgNo = 70

#------------Path of the images --------------------------------------------------------------

dataset_path = "./input/"

original_imgs_train = "./test/256/images3456/"
groundTruth_imgs_train = "./test/256/masks3456/"

#---------------------------------------------------------------------------------------------


def get_datasets(Nimgs, imgs_dir, groundTruth_dir,train_test="null"):
    imgs = np.empty((Nimgs,height,width,channels))
    groundTruth = np.empty((Nimgs, height, width))
    groundArray = np.zeros((Nimgs, 4))
    

    for path, subdirs, files in os.walk(imgs_dir): #list all files, directories in the path
        
        class_index = -1
        class_string = ""
        for i in range(len(files)):

              print "original image: " +files[i]
              img = Image.open(imgs_dir+files[i])
            
              #print img.shape, imgs[i-1].shape
              imgs[i] = np.asarray(img)

              fname_only = os.path.splitext(os.path.basename(files[i]))[0]
              groundTruth_name = fname_only + "_mask.gif"

              print "ground truth name: " + groundTruth_name
              g_truth = Image.open(groundTruth_dir + groundTruth_name)


              print "g_truth max: " + str(np.max(g_truth)) + "min : " + str(np.min(g_truth))
              groundTruth[i] = np.asarray(g_truth)

              #corresponding ground truth
              class_string = fname_only.split("_")
              class_string = class_string[0]
              class_string = class_string.replace('A', '')

              class_index = int(class_string)
              class_index = class_index - 3
              groundArray[i][class_index] = 1	
              print "ground truth classification: " 
              print groundArray[i]
		
              print "input img size: " + str(np.shape(imgs[i])) + " mask img size: " + str(np.shape(groundTruth[i]))
               
              groundTruth[i] = np.where(groundTruth[i] >= 0.2 * np.max(g_truth), 255, 0)

              class_index = -1
              class_string = ""


    print "imgs max: " + str(np.max(imgs))
    print "imgs min: " + str(np.min(imgs))
    print "ground truth max: " + str(np.max(groundTruth))
    print "ground truth min: " + str(np.min(groundTruth))


   
    imgs = np.transpose(imgs, (0, 3, 1, 2))
    assert(imgs.shape == (Nimgs, channels, height, width))
    
    groundTruth = np.reshape(groundTruth, (Nimgs, 1, height, width))
    assert(groundTruth.shape == (Nimgs, 1, height, width))


    return imgs, groundArray, groundTruth

if not os.path.exists(dataset_path):
    os.makedirs(dataset_path)


imgs_train, groundArray_train, groundTruth_train = get_datasets(imgNo, original_imgs_train,groundTruth_imgs_train,"train")

print "saving train datasets"
write_hdf5(imgs_train, dataset_path + "angio_dataset_imgs_derma_train.hdf5")
write_hdf5(groundArray_train, dataset_path + "angio_dataset_groundArray_derma_train.hdf5")
write_hdf5(groundTruth_train, dataset_path + "angio_dataset_groundTruth_derma_train.hdf5")



