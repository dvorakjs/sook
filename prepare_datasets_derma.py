#==========================================================
#
#  This prepare the hdf5 datasets of the database
#
#============================================================

import os
import h5py
import numpy as np
from PIL import Image



def write_hdf5(arr,outfile):
  with h5py.File(outfile,"w") as f:
    f.create_dataset("image", data=arr, dtype=arr.dtype)

channels = 3
height = 256		# 512x512 is the size of original images
width = 256
imgNo = 277

#------------Path of the images --------------------------------------------------------------

dataset_path = "./input/"

original_imgs_train = "./training/256/images/"
groundTruth_imgs_train = "./training/256/masks/"

#---------------------------------------------------------------------------------------------


def get_datasets(Nimgs, imgs_dir, groundTruth_dir,train_test="null"):
    imgs = np.empty((Nimgs,height,width,channels))
    groundTruth = np.zeros((Nimgs, 4))
    

    tmp = 0
    label = 0        # 'negative' directory appears first, so label 0 is set to the negative
    index_accumulated = 0

    print imgs_dir

    for path, subdirs, files in os.walk(imgs_dir): #list all files, directories in the path

        print '\nFolder: '+ path

        if files:
       
          for i in range(len(files)):

              full_filenm = os.path.join(path, files[i])
              print "original image: " +files[i]
              img = Image.open(full_filenm)
            
              #print img.shape, imgs[i-1].shape
              imgs[index_accumulated + i] = np.asarray(img)

              #corresponding ground truth
              label_index = 0
              label = path.split(os.sep)
              label_index = int(label[len(label)-1])-3
              groundTruth[index_accumulated + i][label_index] = 1				
		
              print "input img size: " + str(np.shape(imgs[index_accumulated + i])) + " mask img class: " + str(groundTruth[index_accumulated + i])

          tmp = len(files)
          print "   label : " + str(groundTruth[index_accumulated+tmp-1])

        index_accumulated = index_accumulated + tmp	    

    #reshaping for my standard tensors
    imgs = np.transpose(imgs,(0,3,1,2))
    assert(imgs.shape == (Nimgs,channels,height,width))			# color img can be read by setting channels

    

    return imgs, groundTruth

if not os.path.exists(dataset_path):
    os.makedirs(dataset_path)


imgs_train, groundTruth_train = get_datasets(imgNo, original_imgs_train,groundTruth_imgs_train,"train")

print "saving train datasets"
write_hdf5(imgs_train, dataset_path + "angio_dataset_imgs_derma_train.hdf5")
write_hdf5(groundTruth_train, dataset_path + "angio_dataset_groundTruth_derma_train.hdf5")



