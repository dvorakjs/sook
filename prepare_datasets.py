#==========================================================
#
#  This prepare the hdf5 datasets of the database
#
#============================================================

import os
import h5py
import numpy as np
from PIL import Image



def write_hdf5(arr,outfile):
  with h5py.File(outfile,"w") as f:
    f.create_dataset("image", data=arr, dtype=arr.dtype)

channels = 3
height = 256		# 512x512 is the size of original images
width = 256
imgNo = 70

#------------Path of the images --------------------------------------------------------------
#
# 1st Unet filter
dataset_path = "./input/"

original_imgs_train = "./test/256/images3456/"
groundTruth_imgs_train = "./test/256/masks3456/"
#original_imgs_train = "./angiograms_LAD/training/images/"
#groundTruth_imgs_train = "./angiograms_LAD/training/masks/"

#original_imgs_train = "./angiograms_LAD/training_resized/images/"
#groundTruth_imgs_train = "./angiograms_LAD/training_resized/masks/"

# test set with LAD marked only
#original_imgs_test = "./test/images/"
original_imgs_test = "./test/256/images3456/"
groundTruth_imgs_test = "./test/256/masks3456/"
#groundTruth_imgs_test = "./test/masks/"
#original_imgs_test = "./angiograms_LAD/test/images/"
#groundTruth_imgs_test = "./angiograms_LAD/test/masks/"
#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------

def get_datasets(Nimgs, imgs_dir, groundTruth_dir,train_test="null"):
    imgs = np.empty((Nimgs,height,width,channels))
    groundTruth = np.empty((Nimgs,height,width))

    for path, subdirs, files in os.walk(imgs_dir): #list all files, directories in the path
        for i in range(len(files)):
            #original
            print "original image: " +files[i]
            img = Image.open(imgs_dir+files[i])
            #print img.shape, imgs[i-1].shape
            imgs[i] = np.asarray(img)

            #corresponding ground truth
	    fname_only = os.path.splitext(os.path.basename(files[i]))[0]
	    groundTruth_name = fname_only + "_mask.gif"				# notice: .png file format does not work, it crashes.
            print "ground truth name: " + groundTruth_name
            g_truth = Image.open(groundTruth_dir + groundTruth_name)
	    #g_truth.show()
	    #print groundTruth_dir + groundTruth_name

	    print "g_truth max: " + str(np.max(g_truth)) + " min: " + str(np.min(g_truth))

	    groundTruth[i] = np.asarray(g_truth) 				
		
	    print "input img size: " + str(np.shape(imgs[i])) + " mask img size: " + str(np.shape(groundTruth[i]))

	    #-----
	    # I'm not sure why maximum of pixels is not 255, anyway the maximum value should be adjusted to 255
	    # Probably, scaling problem happens when image size reduction processes. 
	    # ----
	    groundTruth[i] = np.where(groundTruth[i] >= 0.2 * np.max(g_truth), 255, 0)

	    #img2 = Image.fromarray(groundTruth[i].astype(np.uint8))	
	    #img2.save(fname_only + '_mask_chk.gif')
	    #print i

			
    print "imgs max: " +str(np.max(imgs))
    print "imgs min: " +str(np.min(imgs))
    print "ground truth max: " +str(np.max(groundTruth))
    print "ground truth min: " +str(np.min(groundTruth))

    #reshaping for my standard tensors
    imgs = np.transpose(imgs,(0,3,1,2))
    assert(imgs.shape == (Nimgs,channels,height,width))			# color img can be read by setting channels
    groundTruth = np.reshape(groundTruth,(Nimgs,1,height,width))	# GIF file format only because masks are of grayscale.
    assert(groundTruth.shape == (Nimgs,1,height,width))
	
    return imgs, groundTruth

if not os.path.exists(dataset_path):
    os.makedirs(dataset_path)
#getting the training datasets

imgs_train, groundTruth_train = get_datasets(imgNo, original_imgs_train,groundTruth_imgs_train,"train")
print "saving train datasets"
write_hdf5(imgs_train, dataset_path + "angio_dataset_imgs_train.hdf5")
write_hdf5(groundTruth_train, dataset_path + "angio_dataset_groundTruth_train.hdf5")

#getting the testing datasets
'''
imgs_test, groundTruth_test = get_datasets(29, original_imgs_test,groundTruth_imgs_test,"test")
print "saving test datasets"
write_hdf5(imgs_test,dataset_path + "angio_dataset_imgs_test.hdf5")
write_hdf5(groundTruth_test, dataset_path + "angio_dataset_groundTruth_test.hdf5")
'''
